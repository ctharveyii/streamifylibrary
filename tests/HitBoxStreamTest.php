<?php
use Gaufrette\Adapter\Local as LocalAdapter;

/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 3/7/16
 * Time: 10:38 PM
 */
class HitBoxStreamTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var \StreamifyLibrary\Services\Twitch\TwitchStream
     */
    private $streamObject;

    public function setup()
    {
        $adapter = new LocalAdapter(__DIR__ . "/testdata/hitbox");
        $filesystem = new \Gaufrette\Filesystem($adapter);
        $json = json_decode($filesystem->read("singlestream.json"))->livestream[0];
        $this->streamObject = new \StreamifyLibrary\Services\HitBox\HitBoxStream(\StreamifyLibrary\Services\StreamingSiteTypeEnum::TWITCH(), $json);
    }

    public function testConstruction()
    {
        $this->assertEquals("Project M", $this->streamObject->getGame());
        $this->assertEquals(69, $this->streamObject->viewers);
        $this->assertEquals(360, $this->streamObject->videoHeight);
        $this->assertEquals("2016-03-08 01:49:56", $this->streamObject->timeStarted);
        $this->assertEquals("AZPM", $this->streamObject->name);
        $this->assertEquals("69", $this->streamObject->getFormattedViewers());
    }

    public function testChannel()
    {
        $hitboxChannel = $this->streamObject->getChannel();
        $this->assertEquals($hitboxChannel->getCreatedAt(), "2015-09-05 13:26:39");
        $this->assertEquals($hitboxChannel->getFollowers(), "1483");
        $this->assertEquals($hitboxChannel->getFormattedFollowers(), "1.48k");
        $this->assertEquals($hitboxChannel->getLink(), "http://www.hitbox.tv/AZPM");
        $this->assertEquals($hitboxChannel->getName(), "AZPM");
        $this->assertEquals($hitboxChannel->getStatus(), "CZPM51Feat. PG|SS, Blue, ilovebagelz, CHiP, Yummy and MORE!");
        $this->assertEquals($hitboxChannel->getUpdatedAt(), "2016-03-08 01:49:56");
    }

}
