<?php

use Gaufrette\Adapter\Local as LocalAdapter;

/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 3/7/16
 * Time: 10:00 PM
 */
class TwitchStreamTest extends PHPUnit_Framework_TestCase
{

    /**
     * @var \StreamifyLibrary\Services\Twitch\TwitchStream
     */
    private $streamObject;

    public function setup()
    {
        $adapter = new LocalAdapter(__DIR__ . "/testdata/twitch");
        $filesystem = new \Gaufrette\Filesystem($adapter);
        $json = json_decode($filesystem->read("singlestream.json"));
        $this->streamObject = new \StreamifyLibrary\Services\Twitch\TwitchStream(\StreamifyLibrary\Services\StreamingSiteTypeEnum::TWITCH(), $json);
    }

    public function testConstruction()
    {
        $this->assertEquals("Counter-Strike: Global Offensive", $this->streamObject->getGame());
        $this->assertEquals(23226, $this->streamObject->viewers);
        $this->assertEquals(720, $this->streamObject->videoHeight);
        $this->assertEquals(60.7894736842, $this->streamObject->fps);
        $this->assertEquals("2016-03-08T01:45:58Z", $this->streamObject->timeStarted);
        $this->assertEquals("playCEVO", $this->streamObject->name);
        $this->assertEquals("23.23k", $this->streamObject->getFormattedViewers());
    }

}
