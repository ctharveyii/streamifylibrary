<?php

/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 3/7/16
 * Time: 11:09 PM
 */
class StreamTest extends PHPUnit_Framework_TestCase
{


    public function testConstruct()
    {
        $stream = new \StreamifyLibrary\StreamStructure\Stream(\StreamifyLibrary\Services\StreamingSiteTypeEnum::TWITCH(), null);
        $this->assertFalse($stream->isOnline());

        $adapter = new \Gaufrette\Adapter\Local(__DIR__ . "/testdata/twitch");
        $filesystem = new \Gaufrette\Filesystem($adapter);
        $json = json_decode($filesystem->read("singlestream.json"));
        $stream = new \StreamifyLibrary\Services\Twitch\TwitchStream(\StreamifyLibrary\Services\StreamingSiteTypeEnum::TWITCH(), $json);

        $this->assertEquals($stream->checkSpeedRunning(), false);
        $this->assertEquals($stream->embedChatLink(), "http://www.twitch.tv/playcevo/chat");
        $this->assertEquals($stream->embedVideoLink(), "http://www.twitch.tv/playcevo/embed?auto_play=false&autoplay=false");
        $this->assertEquals($stream->getIcon(), '<i class="fa fa-fw fa-twitch"></i>');
        $this->assertEquals($stream->getType(), \StreamifyLibrary\Services\StreamingSiteTypeEnum::TWITCH());
    }

}
