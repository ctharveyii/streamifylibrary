<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/23/15
 * Time: 5:51 AM
 */

namespace StreamifyLibrary\StreamStructure;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;

class ChannelInfo
{
    private $languageTranslate = [
        "US" => "EN",
        "UK" => "EN"
    ];

    public $mature;

    public $partner;

    public $language;

    /**
     * @return boolean
     */
    public function getMature()
    {
        return $this->mature;
    }

    /**
     * @param boolean $mature
     */
    public function setMature($mature)
    {
        $this->mature = $mature;
    }

    /**
     * @return boolean
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param boolean $partner
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
    }

    /**
     * @return String
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param String $language
     */
    public function setLanguage($language)
    {
        if (array_key_exists(strtoupper($language), $this->languageTranslate))
            $language = $this->languageTranslate[$language];
        $this->language = strtolower($language);
    }


    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }
}