<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/23/15
 * Time: 6:01 AM
 */

namespace StreamifyLibrary\StreamStructure;

class ChannelImages
{
    public $logo;

    public $banner;

    public $video_banner;

    public $background;

    public $profileBannerBackgroundColor;

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return mixed
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * @param mixed $banner
     */
    public function setBanner($banner)
    {
        $this->banner = $banner;
    }

    /**
     * @return mixed
     */
    public function getVideoBanner()
    {
        return $this->video_banner;
    }

    /**
     * @param mixed $video_banner
     */
    public function setVideoBanner($video_banner)
    {
        $this->video_banner = $video_banner;
    }

    /**
     * @return mixed
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param mixed $background
     */
    public function setBackground($background)
    {
        $this->background = $background;
    }

    /**
     * @return mixed
     */
    public function getProfileBannerBackgroundColor()
    {
        return $this->profileBannerBackgroundColor;
    }

    /**
     * @param mixed $profileBannerBackgroundColor
     */
    public function setProfileBannerBackgroundColor($profileBannerBackgroundColor)
    {
        $this->profileBannerBackgroundColor = $profileBannerBackgroundColor;
    }


    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }
}