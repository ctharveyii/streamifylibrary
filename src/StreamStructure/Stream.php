<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/23/15
 * Time: 8:43 AM
 */

namespace StreamifyLibrary\StreamStructure;


use Cache\Adapter\Common\CacheItem;
use StreamifyLibrary\Search\Criteria\SpeedRunningCriteria;
use StreamifyLibrary\Services\StreamingSiteTypeEnum;

class Stream
{

    public $type;

    use StreamTraits;

    /**
     * @param $type StreamingSiteTypeEnum
     * @param $json
     */
    public function __construct($type, $json)
    {
        $this->type = $type->getKey();
        var_dump($this->type);
        if ($json == null) {
            $this->online = false;
            return;
        }
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        $array = (array)$this;
        $array['preview'] = $this->preview->toArray();
        $array['channel'] = $this->channel->toArray();
        return $array;
    }

    public function getIcon()
    {

        return $this->getType()->getIcon();
    }

    public function checkSpeedRunning()
    {
        $this->channel->speedrunner = SpeedRunningCriteria::isSpeedRunner($this->getChannel()->getName());
    }

    /**
     * @return StreamingSiteTypeEnum
     */
    public function getType()
    {
        return StreamingSiteTypeEnum::values()[$this->type];
    }

    public function embedVideoLink()
    {
        return $this->getType()->getEmbedVideoLink($this->channel->getName());
    }

    public function embedChatLink()
    {
        return $this->getType()->getEmbedChatLink($this->channel->getName());
    }

}
