<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 10/27/15
 * Time: 9:17 PM
 */

namespace StreamifyLibrary\StreamStructure;


class Game
{

    private $viewers;

    private $channels;

    private $name;

    private $image;

    /**
     * Game constructor.
     * @param $viewers
     * @param $channels
     * @param $name
     * @param $image
     */
    public function __construct($viewers, $channels, $name, $image)
    {
        $this->viewers = $viewers;
        $this->channels = $channels;
        $this->name = $name;
        $this->image = $image;
    }


    /**
     * @return mixed
     */
    public function getViewers()
    {
        return $this->viewers;
    }

    /**
     * @return mixed
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }



}