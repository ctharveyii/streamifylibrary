<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/14/15
 * Time: 11:26 PM
 */

namespace StreamifyLibrary\StreamStructure;


use StreamifyLibrary\Services\StreamingSiteTypeEnum;

class Channel
{
    use ChannelTraits;

    public $type;

    public function __construct(StreamingSiteTypeEnum $siteType, $json)
    {
        $this->type = $siteType;
    }

    protected function getData($json, $string, $default = "")
    {
        if (property_exists($json, $string)) {
            return $json->$string;
        }
        return $default;

    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        $array = (array)$this;
        $array['channelImages'] = $this->channelImages->toArray();
        $array['channelInfo'] = $this->channelInfo->toArray();
//        $array['viewers'] = $this->viewers->toArray();
        return $array;
    }

    /**
     * @return StreamingSiteTypeEnum
     */
    public function getType()
    {
        return $this->type;
    }



}
