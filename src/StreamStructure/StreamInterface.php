<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/14/15
 * Time: 9:45 PM
 */

namespace StreamifyLibrary\StreamStructure;


use Illuminate\Contracts\Support\Arrayable;

interface StreamInterface extends Arrayable
{

    public function getName();

    public function getViewers();

    public function getGame();

    public function getTimeStarted();

    public function getVideoHeight();

    public function getFPS();

    public function getPreview();

    public function getChannel();

    public function isSpeedRunner();


}