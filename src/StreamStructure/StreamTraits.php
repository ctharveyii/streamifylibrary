<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/15/15
 * Time: 12:44 AM
 */

namespace StreamifyLibrary\StreamStructure;

trait StreamTraits
{

    public $viewers;

    public $game;

    public $timeStarted;

    public $videoHeight;

    public $fps;

    public $preview;

    public $channel;

    public $name;

    public $online = true;

    /**
     * @return mixed
     */
    public function getViewers()
    {
        return $this->viewers;
    }

    private function formatNumber($number) {
        if ($number < 1000) {
            return $number;
        }
        if ($number > 1000 && $number < 1000000) {
            $number = round($number / 1000, 2);
            return $number . "k";
        }
        if ($number > 1000000 && $number < 1000000000) {
            $number = round($number / 1000000, 2);
            return $number . "M";
        }
        $number = round($number / 1000000000, 2);
        return $number . "B";
    }

    public function getFormattedViewers(){
        return $this->formatNumber($this->viewers);
    }

    /**
     * @return mixed
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @return Carbon
     */
    public function getTimeStarted()
    {
        return $this->timeStarted;
    }

    /**
     * @return mixed
     */
    public function getVideoHeight()
    {
        return $this->videoHeight;
    }

    /**
     * @return mixed
     */
    public function getFps()
    {
        return ceil($this->fps);
    }

    /**
     * @return Preview
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @return Channel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @return boolean
     */
    public function isOnline()
    {
        return $this->online;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }



}