<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 11/3/15
 * Time: 1:36 PM
 */

namespace StreamifyLibrary\StreamStructure;

use Carbon\Carbon;
use MyCLabs\Enum\Enum;


/**
 * @method static GameTimeEnum DAY()
 * @method static GameTimeEnum WEEK()
 * @method static GameTimeEnum MONTH()
 * @method static GameTimeEnum YEAR()
 * @method static GameTimeEnum ALL()
 **/
class GameTimeEnum extends Enum
{

    const DAY = "day";
    const WEEK = "week";
    const MONTH = "month";
    const YEAR = "year";
    const ALL = "all";

    public function getCarbon()
    {
        switch ($this) {
            case GameTimeEnum::DAY():
                return Carbon::now()->subDay();
            case GameTimeEnum::WEEK():
                return Carbon::now()->subWeek();
            case GameTimeEnum::MONTH():
                return Carbon::now()->subMonth();
            case GameTimeEnum::YEAR():
                return Carbon::now()->subYear();
            case GameTimeEnum::ALL():
                return null;
        }
    }

    public static function getByValue($value)
    {
        $value = strtolower($value);
        foreach (GameTimeEnum::values() as $enum) {
            if ($enum->getValue() === $value) {
                return $enum;
            }
        }
        return null;
    }

}