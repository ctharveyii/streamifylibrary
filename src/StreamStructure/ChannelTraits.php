<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/15/15
 * Time: 12:55 AM
 */

namespace StreamifyLibrary\StreamStructure;

use StreamifyLibrary\StreamStructure\ChannelImages;

trait ChannelTraits
{
    public $name = "";

    public $link;

    public $status;

    public $createdAt;

    public $updatedAt;

    public $views;

    public $followers;

    public $channelInfo;

    public $channelImages;

    public $speedrunner = false;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }

    public function getFormattedViews(){
        return formatNumber($this->views);
    }

    /**
     * @return mixed
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    public function getFormattedFollowers(){
        return $this->formatNumber($this->followers);
    }

    /**
     * @return ChannelInfo
     */
    public function getChannelInfo()
    {
        return $this->channelInfo;
    }

    /**
     * @return ChannelImages
     */
    public function getChannelImages()
    {
        return $this->channelImages;
    }

    private function formatNumber($number) {
        if ($number < 1000) {
            return $number;
        }
        if ($number > 1000 && $number < 1000000) {
            $number = round($number / 1000, 2);
            return $number . "k";
        }
        if ($number > 1000000 && $number < 1000000000) {
            $number = round($number / 1000000, 2);
            return $number . "M";
        }
        $number = round($number / 1000000000, 2);
        return $number . "B";
    }
}