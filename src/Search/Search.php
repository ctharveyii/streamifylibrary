<?php

namespace StreamifyLibrary\Search;

use Cache\Adapter\Common\AbstractCachePool;
use StreamifyLibrary\Services\StreamingSiteTypeEnum;

/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 3/7/16
 * Time: 2:03 AM
 */
class Search
{


    /**
     * Search constructor.
     * @param AbstractCachePool $cache
     */
    public function __construct($streamingServices, $cache = null)
    {
        $this->services = $streamingServices;
        $this->cache = $cache;
    }

    public function getAllTopGames()
    {
        $storedGames = [];
        $channels = [];
        foreach ($this->services as $service) {
            /* @var $service StreamingSiteConnection */
            $games = $service->getTopGames();
            foreach ($games as $game) {
                /* @var $game Game */
                if (!isset($storedGames[$game->getName()])) {
                    $storedGames[$game->getName()] = 0;
                }
                if (!isset($channels[$game->getName()])) {
                    $channels[$game->getName()] = 0;
                }

                $storedGames[$game->getName()] += $game->getViewers();
                $channels[$game->getName()] += $game->getChannels();
            }
        }
        return ["games" => $storedGames, "channels" => $channels];
    }

    public function getGameFromAllServices($gameName, $page = 0, $count = 10)
    {
        $streams = [];
        $streamsLoaded = [];
        foreach ($this->services as $service) {
            if (isset($streamsLoaded[$service->getName()])) {
                $endChannel = end($streamsLoaded[$service->getName()]);
            }
            $streamsLoaded[$service->getName()] = $service->getGame($gameName, $page);
            if (is_array($streamsLoaded)[$service->getName()]) {
                $streams = array_merge($streamsLoaded[$service->getName()], $streams);
            }
            /* @var $channel Channel */
            $channel = end($streams);
        }
        return $streams;

    }

    /**
     * @param $type \StreamifyLibrary\Services\StreamingSiteTypeEnum
     * @param $name string
     * @param bool $flush
     * @return \StreamifyLibrary\Channel
     */
    public function getChannel($type, $name, $flush = false)
    {
        return $type->getStreamingSite()->getChannel($name, $type, $flush);
    }

    /**
     * @param $type \StreamifyLibrary\Services\StreamingSiteTypeEnum
     * @param $streamName
     * @param $flush
     * @return \StreamifyLibrary\Stream
     */

    public function getStream($type, $streamName, $flush)
    {
        return $type->getStreamingSite()->getStream($streamName, $type, $flush);
    }

    //TODO needs to be cleaned up too many lines in one function

    /**
     * @param array $criterias
     * @param int $count
     * @param int $cacheTime
     * @param string $id
     * @param bool|false $flush
     * @return array|mixed|void
     */

    public function getByCriteria($criterias, $count = 10, $cacheTime = 0, $id = null, $flush = false)
    {
        $streams = null;
        $serviceSearchResults = null;
        if ($id !== 0 && isset($id) && !$flush) {
            $serviceSearchResults = $this->cache->getItem($id);
        }
        if (array_key_exists("site", $criterias) && is_array($criterias['site']['value'])) {
            $streams = array_map("strtolower", $criterias['site']['value']);
        } else if (array_key_exists("site", $criterias)) {
            $streams = [$criterias['site']['value']];
        } else {
            $streams = StreamingSiteTypeEnum::values();
        }
        unset($criterias["site"]);
        $savedStreams = [];
        $currentCount = 0;
        $nullCount = array();
        if ($serviceSearchResults == null)
            $serviceSearchResults = $this->getStreams($criterias, $streams, $flush);
        while ($currentCount < $count && sizeof($nullCount) != sizeof($streams)) {
            $currentHigh = null;
            foreach ($serviceSearchResults as $service => $serviceResults) {
                $connection = $this->services[strtolower($service)];
                /* @var $serviceResults SearchResults */
                if ($serviceResults->getCurrentStream() != null) {
                    if ($serviceResults->getCurrentStream()->getViewers() >= $currentHigh) {
                        $currentHigh = $serviceResults->getCurrentStream()->getViewers();
                        $savedStreams[] = $serviceResults->getCurrentStream();
                        $serviceResults->getNext($connection);
                        $currentCount++;
                    }
                } else {
                    $nullCount[$service] = null;
                }
            }
        }
        $searchId = "search_" . uniqid();
        $this->cache->save(new SearchResults($searchId, new DateInterval($cacheTime), $serviceSearchResults));
        return ['id' => $searchId, 'results' => $savedStreams];
    }

    public function getStreams($criterias, $streams, $flush)
    {
        $serviceSearchResults = [];
        foreach ($this->services as $service) {
            /* @var $service StreamingSiteConnection */
            if (in_array(strtolower($service->getName()), $streams)) {
                $serviceSearchResults[strtolower($service->getName())] = $service->getByCriteria($criterias, $flush);
            }
        }
        return $serviceSearchResults;
    }
}