<?php

namespace StreamifyLibrary\Search;

use StreamifyLibrary\ServiceSearchResults;


/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 3/7/16
 * Time: 2:09 AM
 */
class SearchResults extends \Cache\Adapter\Common\CacheItem
{

    /**
     * @var ServiceSearchResults[]
     */
    private $results = [];

    /**
     * SearchResults constructor.
     * @param string $key
     * @param DateInterval $ttl
     * @param ServiceSearchResults[] $results
     */
    public function __construct($key, DateInterval $ttl, array $results)
    {
        parent::__construct($key);
        $this->results = $results;
        if ($ttl == null || $ttl->s < 1) {
            $ttl = new DateInterval("300s");
        }
        $this->expiresAfter($ttl);
    }
}