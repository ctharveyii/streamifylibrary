<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 10/30/15
 * Time: 10:01 PM
 */

namespace StreamifyLibrary\Search;


use Doctrine\Common\Collections\ArrayCollection;

class Results
{

    private $results;

    private $currentLink;

    private $nextLink;

    /**
     * Results constructor.
     * @param $results
     * @param $currentLink
     * @param $nextLink
     */
    public function __construct($results, $currentLink, $nextLink)
    {
        $this->results = new ArrayCollection($results);
        $this->currentLink = $currentLink;
        $this->nextLink = $nextLink;
    }

    /**
     * @return mixed
     */
    public function getResults()
    {
        if ($this->results->isEmpty()) return null;
        return $this->results;
    }

    /**
     * @return mixed
     */
    public function getCurrentLink()
    {
        return $this->currentLink;
    }

    /**
     * @return mixed
     */
    public function getNextLink()
    {
        return $this->nextLink;
    }

    /**
     * @return boolean
     */
    public function hasResults()
    {
        return !$this->results->isEmpty();
    }

}