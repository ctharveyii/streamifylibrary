<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 6/26/15
 * Time: 3:44 PM
 */

namespace StreamifyLibrary\Search\Criteria;


use StreamifyLibrary\Stream;
use StreamifyLibrary\Search\ViewersTypeEnum;

class UserCriteria extends CriteriaBase
{

    public function __construct()
    {
        parent::__construct("userin", "Find a User", CriteriaEnum::USERIN());
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        $stream->channel->getViewers();
        $viewers = $stream->channel->viewers->getViewers(ViewersTypeEnum::ALL());
        foreach ($viewers as $viewer) {
            if (strtolower($viewer) === strtolower($value)) {
                return true;
            }
        }
        return false;
    }

    public function getHTML($value)
    {
        return "";
    }
}