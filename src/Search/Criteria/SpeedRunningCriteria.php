<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 11:11 AM
 */

namespace StreamifyLibrary\Search\Criteria;


use StreamifyLibrary\Services\StreamingSiteTypeEnum;
use StreamifyLibrary\Services\Twitch\TwitchService;
use StreamifyLibrary\Stream;

class SpeedRunningCriteria extends AbsCheckboxCriteriaBase
{

    private static $json;
    private static $channels = [];
    private static $init = false;

    public function __construct()
    {
        parent::__construct("speedrunning", "Speedrunners", CriteriaEnum::SPEEDRUNNING());
        SpeedRunningCriteria::init();
    }

    /**
     * @param Stream $stream
     * @return boolean
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        if ($stream->getType()->getValue() === 0
            && in_array(
                strtolower($stream->channel->getName()), SpeedRunningCriteria::$channels)
        ) {
            return true;
        }
        return false;
    }

    public static function isSpeedRunner($stream)
    {
        SpeedRunningCriteria::init();
        return (in_array(strtolower($stream), SpeedRunningCriteria::$channels));
    }

    public static function init()
    {
        if (!SpeedRunningCriteria::$init) {
            $twitch = new TwitchService();
            $json = json_decode($twitch->get("http://api.speedrunslive.com/frontend/streams", 300));
            SpeedRunningCriteria::$json = $json->_source->channels;
            SpeedRunningCriteria::$init = true;
        }
        foreach (SpeedRunningCriteria::$json as $channel) {
            SpeedRunningCriteria::$channels[] = strtolower($channel->name);
        }
    }
}