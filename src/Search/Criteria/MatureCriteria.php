<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 10:35 AM
 */

namespace StreamifyLibrary\Search\Criteria;




use StreamifyLibrary\Stream;

class MatureCriteria extends AbsCheckboxCriteriaBase
{

    public function __construct()
    {
        parent::__construct("mature", "Mature", CriteriaEnum::MATURE());
    }

    /**
     * @param Stream $stream
     * @return boolean
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
        return $value === $stream->channel->getChannelInfo()->getMature();
//        if($value){
//            varDump($stream->channel->getChannelInfo()->getMature());
//            if($stream->channel->getChannelInfo()->getMature()) return true;
//        }
//        if(!$value){
//            echo "Mature ";
//            varDump($value);
//            varDump($stream->channel->getChannelInfo()->getMature());
//            if(!$stream->channel->getChannelInfo()->getMature()) return true;
//        }
//        return false;
    }
}