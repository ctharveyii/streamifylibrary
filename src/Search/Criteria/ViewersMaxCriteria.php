<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 12:22 PM
 */

namespace StreamifyLibrary\Search\Criteria;



use StreamifyLibrary\Stream;

class ViewersMaxCriteria extends AbsTextCriteriaBase
{

    public function __construct()
    {
        parent::__construct("viewersmax", "Max Viewers", CriteriaEnum::VIEWERSMAX());
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        if ($stream->viewers === -1) {
            $twitch = new Twitch();
            $stream = $twitch->getStream($stream->channel->name, "flush");
        }
        return $stream->viewers < $value;
    }
}