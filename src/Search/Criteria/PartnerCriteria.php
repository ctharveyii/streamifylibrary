<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 10:29 AM
 */

namespace StreamifyLibrary\Search\Criteria;




use StreamifyLibrary\Stream;

class PartnerCriteria extends AbsCheckboxCriteriaBase
{

    public function __construct()
    {
        parent::__construct("Partner", "Partner", CriteriaEnum::PARTNER());
    }

    /**
     * @param Stream $stream
     * @return boolean
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
        if ($value) {
            return $stream->channel->getChannelInfo()->getPartner();
        }
        return !$stream->channel->getChannelInfo()->getPartner();
    }
}