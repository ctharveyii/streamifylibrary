<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 11:32 AM
 */

namespace StreamifyLibrary\Search\Criteria;




use StreamifyLibrary\Stream;

class ProsCriteria extends AbsCheckboxCriteriaBase
{

    public function __construct()
    {
        parent::__construct("pros", "Pro Players", CriteriaEnum::PROS());
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     * @throws Exception
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        throw new Exception("Not implemented yet.");
    }
}