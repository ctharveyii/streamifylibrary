<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 12:33 PM
 */

namespace StreamifyLibrary\Search\Criteria;




use StreamifyLibrary\Stream;

class StatusCriteria extends AbsTextCriteriaBase
{

    public function __construct()
    {
        parent::__construct("status", "Status", CriteriaEnum::STATUS());
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        $status = strtolower($stream->channel->getStatus());
        $value = strtolower($value);
        if (strpos($status, $value) !== false) {
            return true;
        }
        return false;
    }
}