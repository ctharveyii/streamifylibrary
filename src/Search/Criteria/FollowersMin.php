<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 12:06 PM
 */

namespace StreamifyLibrary\Search\Criteria;



use StreamifyLibrary\Stream;

class FollowersMin extends AbsTextCriteriaBase
{

    public function __construct()
    {
        parent::__construct("followersmin", "Min Followers", CriteriaEnum::FOLLOWERSMIN());
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        if ($stream->channel->getFollowers() === -1) {
            $twitch = new Twitch();
            $stream = $twitch->getStream($stream->channel->name, "flush");
        }
        if ($stream->channel->getFollowers() > $value) {
            return true;
        }
        return false;
    }
}