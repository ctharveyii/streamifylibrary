<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 9:17 AM
 */

namespace StreamifyLibrary\Search\Criteria;


use StreamifyLibrary\Stream;

class LanguageCriteria extends AbsSelectCriteriaBase
{

    public static $LANGUAGES = [
        'en',
        'da',
        'de',
        'es',
        'fr',
        'it',
        'hu',
        'nl',
        'no',
        'pl',
        'pt',
        'sk',
        'fi',
        'sv',
        'vi',
        'tr',
        'cs',
        'bg',
        'ru',
        'ar',
        'th',
        'zh',
        'ja',
        'ko',
        'other'];

    public static $LANGUAGEINDEX = [
        "all" => "All",
        'en' => "English",
        'da' => "Dansk",
        'de' => "Deutsch",
        'es' => "Español",
        "fr" => "Français",
        "it" => "Italiano",
        "hu" => "Magyar",
        "nl" => "Nederlands",
        "no" => "no",
        "pl" => "Polski",
        "pt" => "Português",
        "sk" => "Slovenčina",
        "fi" => "Suomi",
        "sv" => "Svenska",
        "vi" => "Tiếng Việt (㗂越)",
        "tr" => "Türkçe",
        "cs" => "Ceština",
        "bg" => "български (Bãlgarski)",
        "ru" => "Русский язык (Russkij jazyk)",
        "ar" => "(al arabiya) العربية",
        "th" => "ภาษาไทย (paasaa-tai)",
        "zh" => "中文 (zhōngwén)",
        "ja" => "日本語 (nihongo)",
        "ko" => "한국어 [韓國語] (han-guk-eo)",
        "other" => "Other"
    ];


    function __construct()
    {
        parent::__construct('language', "Language", CriteriaEnum::LANGUAGE(), LanguageCriteria::$LANGUAGEINDEX);
    }

    /**
     * @param Stream $stream
     * @param $values
     * @return bool
     * @throws \Exception
     */
    public
    function meetsCriteria(Stream $stream, $values)
    {
        if (!is_array($values)) {
            $values = explode(",", $values);
        }
        foreach ($values as $value) {

            $value = strtolower($value);
            if ($value == "all") {
                return true;
            }
            if (!in_array($value, LanguageCriteria::$LANGUAGES)) {
                throw new \Exception("Invalid language $value");
            }
            if ($stream->channel->getChannelInfo()->getLanguage() === $value) {
                return true;
            }
        }
        return false;
    }

    public function getHTML($value, $class = null)
    {
        $name = $this->getName();
        $displayName = $this->getDisplayName();
        $html = view('Search/Filters/LanguageFilter', ['name' => $name, 'displayName' => $displayName, 'iterable' => $this->iterable])->render();
        return $html;
    }


}