<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 10:35 AM
 */

namespace StreamifyLibrary\Search\Criteria;


use StreamifyLibrary\Stream;

class SiteCriteria extends AbsSelectCriteriaBase
{

    public static $SITES = ['hitbox' => '<img src="img/hitbox-icon.svg" alt="hitbox" width="18" height="18"/>', 'twitch' => '<i class="fa fa-twitch"></i>'];

    public function __construct()
    {
        parent::__construct("site", "Sites", CriteriaEnum::SITE(), array_keys(SiteCriteria::$SITES));
    }

    /**
     * @param Stream $stream
     * @return boolean
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        return true;
    }
}