<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 10:37 AM
 */

namespace StreamifyLibrary\Search\Criteria;


use StreamifyLibrary\Stream;

class GameCriteria extends AbsTextCriteriaBase
{

    public function __construct()
    {
        parent::__construct("game", "Game", CriteriaEnum::GAME());
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        $values = explode(",", $value);
        foreach ($values as $value) {
            $value = strtolower($this->removeAccent($value));
            if ($stream->game === null) return false;
            $game = strtolower(str_replace(" ", " ", $this->removeAccent($stream->game)));
            if ($game === $value) {
                return true;
            }
            if (strpos($game, $value) !== false) {
                return true;
            }
        }
        return false;
    }

    public function removeAccent($string)
    {
        if (strpos($string = htmlentities($string, ENT_QUOTES, 'UTF-8'), '&') !== false) {
            $string = html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i', '$1', $string), ENT_QUOTES, 'UTF-8');
        }

        return $string;
    }
}