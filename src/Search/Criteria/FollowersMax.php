<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 11:43 AM
 */

namespace StreamifyLibrary\Search\Criteria;



use StreamifyLibrary\Stream;

class FollowersMax extends AbsTextCriteriaBase
{

    public function __construct()
    {
        parent::__construct("followersmax", "Max Followers", CriteriaEnum::FOLLOWERSMAX());
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        if ($stream->channel->getFollowers() === -1) {
            $twitch = new Twitch();
            $stream = $twitch->getStream($stream->channel->name, "flush");
        }
        if ($stream->channel->getFollowers() < $value) {
            return true;
        }
        return false;
    }
}