<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/7/15
 * Time: 3:31 PM
 */

namespace StreamifyLibrary\Search\Criteria;


abstract class AbsSelectCriteriaBase extends CriteriaBase
{
    public function __construct($name, $displayName, $criteriaEnum, $iterable)
    {
        parent::__construct($name, $displayName, $criteriaEnum);
        $this->iterable = $iterable;
    }

    public function getHTML($value, $class = null)
    {
        $name = $this->getName();
        $displayName = $this->getDisplayName();
        $html = view('Search/Filters/SelectCriteriaFilter', ['name' => $name, 'displayName' => $displayName, 'iterable' => $this->iterable])->render();
        return $html;
    }
}