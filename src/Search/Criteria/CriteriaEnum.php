<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 9:03 AM
 */

namespace StreamifyLibrary\Search\Criteria;


use MyCLabs\Enum\Enum;

/**
 * @method static CriteriaEnum LANGUAGE()
 * @method static CriteriaEnum MATURE()
 * @method static CriteriaEnum PARTNER()
 * @method static CriteriaEnum GAME()
 * @method static CriteriaEnum SPEEDRUNNING()
 * @method static CriteriaEnum PROS()
 * @method static CriteriaEnum NAME()
 * @method static CriteriaEnum FOLLOWERSMAX()
 * @method static CriteriaEnum FOLLOWERSMIN()
 * @method static CriteriaEnum FPSMIN()
 * @method static CriteriaEnum VIEWERSMAX()
 * @method static CriteriaEnum VIEWERSMIN()
 * @method static CriteriaEnum USERIN()
 * @method static CriteriaEnum SITE()
 */
class CriteriaEnum extends Enum
{
    const LANGUAGE = 'language';
    const MATURE = 'mature';
    const PARTNER = 'partner';
    const GAME = 'game';
    const SPEEDRUNNING = 'speedrunning';
    const PROS = 'pros';
    const NAME = 'name';
    const FOLLOWERSMAX = 'followersmax';
    const FOLLOWERSMIN = 'followersmin';
    const FPSMIN = 'fpsmin';
    const VIEWERSMAX = 'viewersmax';
    const VIEWERSMIN = 'viewersmin';
    const STATUS = "status";
    const USERIN = "userin";
    const SITE = 'site';

    private $languageCriteria;

    /**
     * @return CriteriaBase
     */
    public function getCriteria()
    {
        switch (strtolower($this->getValue())) {
            case "language":
                if (!isset($this->languageCriteria)) {
                    $this->languageCriteria = new LanguageCriteria();
                }
                return $this->languageCriteria;
            case "partner":
                if (!isset($this->partner)) {
                    $this->partner = new PartnerCriteria();
                }
                return $this->partner;
            case "mature":
                if (!isset($this->mature)) {
                    $this->mature = new MatureCriteria();
                }
                return $this->mature;
            case "game":
                if (!isset($this->game)) {
                    $this->game = new GameCriteria();
                }
                return $this->game;
            case "speedrunning":
                if (!isset($this->speedRunning)) {
                    $this->speedRunning = new SpeedRunningCriteria();
                }
                return $this->speedRunning;
            case "pros":
                if (!isset($this->pros)) {
                    $this->pros = new ProsCriteria();
                }
                return $this->pros;
            case "name":
                if (!isset($this->nameCriteria)) {
                    $this->nameCriteria = new NameCriteria();
                }
                return $this->nameCriteria;
            case "followersmax":
                if (!isset($this->followersMax)) {
                    $this->followersMax = new FollowersMax();
                }
                return $this->followersMax;
            case "followersmin":
                if (!isset($this->followersMin)) {
                    $this->followersMin = new FollowersMin();
                }
                return $this->followersMin;
            case "fpsmin":
                if (!isset($this->fps)) {
                    $this->fps = new FPSCriteria();
                }
                return $this->fps;
            case "viewersmax":
                if (!isset($this->viewersMax)) {
                    $this->viewersMax = new ViewersMaxCriteria();
                }
                return $this->viewersMax;
            case "viewersmin":
                if (!isset($this->viewersMin)) {
                    $this->viewersMin = new ViewersMinCriteria();
                }
                return $this->viewersMin;
            case "status":
                if (!isset($this->status)) {
                    $this->status = new StatusCriteria();
                }
                return $this->status;
            case "userin":
                if (!isset($this->userIn)) {
                    $this->userIn = new UserCriteria();
                }
                return $this->userIn;
            case "site":
                if (!isset($this->site)) {
                    $this->site = new SiteCriteria();
                }
                return $this->site;
        }
    }

    /**
     * @param string $criteria
     * @return CriteriaEnum
     */
    public static function byName($criteria)
    {
        $criteria = strtoupper($criteria);
//        if ($criteria === null || $criteria === "") {
//            return CriteriaEnum::VIEWERSMIN();
//        }
        try {
            $criteria = CriteriaEnum::$criteria();
        } catch (\BadMethodCallException $ex) {
            return null;
        }
        return $criteria;
    }

}