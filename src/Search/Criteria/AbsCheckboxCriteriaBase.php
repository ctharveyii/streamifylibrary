<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/7/15
 * Time: 3:32 PM
 */

namespace StreamifyLibrary\Search\Criteria;


abstract class AbsCheckboxCriteriaBase extends CriteriaBase
{
    public function getHTML($value, $class = null)
    {
        $name = $this->getName();
        $displayName = $this->getDisplayName();
        $html = view('Search/Filters/CheckboxFilter', ['displayName' => $displayName, 'name' => $name, 'class' => $class])->render();
        return $html;
    }


}