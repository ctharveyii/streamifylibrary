<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 11:33 AM
 */

namespace StreamifyLibrary\Search\Criteria;


use StreamifyLibrary\Stream;

class NameCriteria extends AbsTextCriteriaBase
{

    public function __construct()
    {
        parent::__construct("name", "Name", CriteriaEnum::NAME());
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        if ($stream->game === null) return false;
        $name = strtolower(str_replace("-", " ", $stream->getChannel()->getName()));
        if ($name === strtolower($value)) {
            return true;
        }
        if (strpos($name, strtolower($value)) !== false) {
            return true;
        }
        return false;

    }
}