<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 9:14 AM
 */

namespace StreamifyLibrary\Search\Criteria;


use StreamifyLibrary\Stream;

abstract class CriteriaBase
{

    private $criteriaEnum;

    private $name;

    private $displayName;

    private $count;

    public function __construct($name, $displayName, CriteriaEnum $enum)
    {
        $this->name = $name;
        $this->criteriaEnum = $enum;
        $this->displayName = $displayName;
    }

    /**
     * @return mixed
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCriteriaEnum()
    {
        return $this->criteriaEnum;
    }

    public function preMeetsCriteria(Stream $stream, $value)
    {
        $this->addCount();
        if ($value === null) {
            return true;
        }
        if ($stream === null) {
            return false;
        }
        try {
            $result = $this->meetsCriteria($stream, $value);
            return $result;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     */
    public abstract function meetsCriteria(Stream $stream, $value);


    public abstract function getHTML($value, $class = null);

    public function addCount()
    {
        $this->count++;
    }

    public function resetCount(){
        $this->count = 0;
    }
}