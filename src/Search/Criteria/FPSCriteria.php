<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/25/15
 * Time: 12:16 PM
 */

namespace StreamifyLibrary\Search\Criteria;




use StreamifyLibrary\Stream;

class FPSCriteria extends AbsTextCriteriaBase
{

    public function __construct()
    {
        parent::__construct("fpsmin", "Avg FPS", CriteriaEnum::FPSMIN());
    }

    /**
     * @param Stream $stream
     * @param $value
     * @return bool
     */
    public function meetsCriteria(Stream $stream, $value)
    {
        if ($value > 60){
            $value = 60;
        }
        return $stream->fps >= $value - 1;
    }
}