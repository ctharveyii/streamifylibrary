<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/7/15
 * Time: 3:31 PM
 */

namespace StreamifyLibrary\Search\Criteria;


abstract class AbsTextCriteriaBase extends CriteriaBase
{

    public function getHTML($value, $class = null)
    {
        $name = $this->getName();
        $displayName = $this->getDisplayName();
        $html = view("Search/Filters/TextInputFilter", ['name' => $name, 'displayName' => $displayName, 'class' => $class])->render();
        return $html;
    }
}