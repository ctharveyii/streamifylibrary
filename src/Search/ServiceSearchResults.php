<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/18/15
 * Time: 7:04 PM
 */


namespace StreamifyLibrary;


use Cache\Adapter\Common\CacheItem;
use StreamifyLibrary\Services\StreamingLink;
use StreamifyLibrary\Services\StreamingSiteConnection;

class ServiceSearchResults
{

    private $connection;

    private $streamsLeft;

    /**
     * @var \Closure
     */
    private $nextLink;

    private $currentStream;

    private $criterias;

    private $streamsRemaining = true;

    private $currentLink;

    private $linkCache;

    private $currentLinkFlush;

    /**
     * SearchResults constructor.
     * @param StreamingSiteConnection $connection
     * @param $criterias
     * @param StreamingLink $currentLink
     * @param bool $flush
     */
    public function __construct(StreamingSiteConnection $connection, $criterias, $currentLink, $flush = false)
    {
        $this->criterias = $criterias;
        $this->flush = $flush;
        $this->currentLink = $currentLink;
        $this->offset = 0;
        $results = $currentLink->getResult($connection, ["offset" => $this->offset]);
        $this->streamsLeft = $connection->getStreams($results->getResults());
        $this->id = uniqid();
        $this->linkCache = $currentLink->getCacheTime();
        $this->currentLinkFlush = $currentLink->isFlush();
        $this->currentLink = $currentLink->getLink();
        $this->nextLink = $results->getNextLink();
        $results->getResults()->streams = [];
        $this->connection = $connection;
        $this->getNext($connection);
    }

    //TODO catch errors and pause/retry
    public function getNext($connection)
    {
        $stream = null;
        $matches = false;
        $this->currentStream = null;
        while ($stream == null || !$matches) {
            $stream = array_shift($this->streamsLeft);
            if ($stream != null) {
                $matches = $connection->matchesCriterias($stream, $this->criterias);
            }
            if (sizeof($this->streamsLeft) === 0) {
                $this->offset++;

                if ($this->nextLink === "" || $this->nextLink === [] || $this->nextLink == null) {
                    $this->currentStream = null;
                    return;
                }
                $results = $this->nextLink->getResult($this->connection);
                $this->nextLink = $results->getNextLink();
                $this->streamsLeft = $connection->getStreams($results->getResults());
                if (count($this->streamsLeft) === 0) return;
            }
        }
        $this->currentStream = $stream;
    }

    /**
     * @return Stream
     */
    public function getCurrentStream()
    {
        return $this->currentStream;
    }

    /**
     * @param $offset
     * @return StreamingLink
     */
    public function getNextLink($connection, $offset, $flush)
    {
        $results = $connection->getLinks()->getPageLink($offset, $flush);
//        $results = $this->nextLink->__invoke($offset, $flush);
        return $results;
    }


}