<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 3/7/16
 * Time: 11:34 PM
 */

namespace StreamifyLibrary;


use Curl\Curl;

class LinkProvider
{

    private static $defaultTtl = 60;

    public static function getLink($link, $header = [], $flush = false, $ttl = null)
    {
        var_dump($link);
        if ($ttl == null) {
            $ttl = LinkProvider::$defaultTtl;
        }
        if ($flush) {
            Cache::forget($link);
        }
        $value = Cache::get($link, function () use ($link, $ttl, $header) {
            $curl = new Curl();
            $curl->setOpt(CURLOPT_ENCODING, "");
            if (is_array($header))
                $curl->setHeader($header[0], $header[1]);
            if ($header != null) {
                $curl->setHeader($header[0], $header[1]);
            }
            $curl->get($link);

            if ($curl->error) {
//                Log::Error($curl->error_message);
//                Log::Error($curl->error_code);
                throw new \Exception("CURL ERROR " . $curl->error_message . " site: " . $link);
            } else {
//                Log::info($string . " " . $time);
                Cache::put(new CurlResponse($link, $curl->response), new \DateInterval("PT".$ttl."S"));
                return $curl->response;
            }
        });
        return $value;
    }

    /**
     * @return int
     */
    public static function getDefaultTtl()
    {
        return self::$defaultTtl;
    }

    /**
     * @param int $defaultTtl
     */
    public static function setDefaultTtl($defaultTtl)
    {
        self::$defaultTtl = $defaultTtl;
    }

}