<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 3/7/16
 * Time: 11:16 PM
 */

namespace StreamifyLibrary;


use Cache\Adapter\Common\AbstractCachePool;
use Cache\Adapter\Common\CacheItem;

class Cache
{
    /**
     * @var AbstractCachePool
     */
    private static $cache;

    public static function noCache()
    {
        return is_null(Cache::$cache);
    }

    public function __construct(AbstractCachePool $cache)
    {
        if (Cache::noCache()) {
            return;
        }
        Cache::$cache = $cache;
    }

    public static function forget($name)
    {
        if (Cache::noCache()) {
            return;
        }
        Cache::$cache->deleteItem($name);
    }

    public static function put(CacheItem $item, \DateInterval $time)
    {
        if (Cache::noCache()) {
            return;
        }
        $item->expiresAfter($time);
        Cache::$cache->save($item);
    }

    public static function get($key, $callback)
    {
        if (Cache::noCache()) {
            return $callback();
        }
        if (Cache::$cache->hasItem($key)) {
            return Cache::$cache->getItem($key);
        } else {
            return $callback();
        }
    }

}