<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 3/7/16
 * Time: 11:33 PM
 */

namespace StreamifyLibrary;


use Cache\Adapter\Common\CacheItem;

class CurlResponse extends CacheItem
{
    private $response;

    public function __construct($link, $response)
    {
        parent::__construct($link);
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

}