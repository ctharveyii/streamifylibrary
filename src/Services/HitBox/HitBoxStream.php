<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/15/15
 * Time: 1:07 AM
 */

namespace StreamifyLibrary\Services\HitBox;


use StreamifyLibrary\Services\StreamingSiteTypeEnum;
use StreamifyLibrary\Services\Twitch;
use StreamifyLibrary\StreamStructure\Stream;

class HitBoxStream extends Stream
{

    /**
     * @param $type StreamingSiteTypeEnum
     * @param $json
     */
    public function __construct($type, $json)
    {
        parent::__construct($type, $json);
        $this->preview = new Twitch\Preview("", "http://edge.sf.hitbox.tv" . $json->media_thumbnail, "http://edge.sf.hitbox.tv" . $json->media_thumbnail_large);
        $this->channel = new HitBoxChannel($type, $json);
        $this->viewers = $json->media_views;
        $this->game = $json->category_name;
        $this->timeStarted = $json->media_live_since;
        $this->name = $json->media_display_name;
        $decoded = json_decode($json->media_profiles);
        if ($decoded != null) {
            $this->videoHeight = end($decoded)->height;
        }
        $this->online = $json->media_is_live == 1;
    }

}