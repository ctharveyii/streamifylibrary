<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/14/15
 * Time: 10:05 PM
 */

namespace StreamifyLibrary\Services;


class StreamingLink
{

    private $link;

    private $cacheTime;
    private $flush;

    private $var;

    /**
     * StreamingLink constructor.
     * @param int $cacheTime
     * @param $nextInterface
     * @param string $link
     * @param bool $flush
     */
    public function __construct($cacheTime, $nextInterface, $link, $flush = false)
    {
        $this->cacheTime = $cacheTime;
        $this->nextInterface = $nextInterface;
        $this->link = $link;
        $this->flush = $flush;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return str_replace($this->var["name"], $this->var['value'], $this->link);
    }

    /**
     * @return int
     */
    public function getCacheTime()
    {
        return $this->cacheTime;
    }

    /**
     * @return boolean
     */
    public function isFlush()
    {
        return $this->flush;
    }

    /**
     * @param StreamingSiteConnection $service
     * @param array $vars
     * @return Results
     */
    public function getResult($service, $vars = null)
    {

        $results = json_decode($service->get($this->getLink(), $this->cacheTime, $this->flush));
        if (isset($this->nextInterface))
            $result = new Results($results, $this->getLink(), $this->nextInterface->getNextLink($this, $results, $vars));
        else
            $result = new Results($results, $this->getLink(), null);
        return $result;
    }

    public function setVar($name, $value)
    {
        $this->var = ["name" => $name, "value" => $value];
        return $this;
    }

}