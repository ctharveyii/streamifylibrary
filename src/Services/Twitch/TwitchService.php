<?php

namespace StreamifyLibrary\Services\Twitch;

use StreamifyLibrary\Services\NextInterface;
use StreamifyLibrary\Services\StreamingLink;
use StreamifyLibrary\Services\StreamingLinks;
use StreamifyLibrary\Services\StreamingSiteConnection;
use StreamifyLibrary\Services\StreamingSiteTypeEnum;
use StreamifyLibrary\ServiceSearchResults;
use StreamifyLibrary\StreamStructure\Game;

/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/23/15
 * Time: 4:03 AM
 */
class TwitchService extends StreamingSiteConnection
{

    private $channelStoreCacheMinutes = 5;

    private $clientID;

    private $clientSecret;

    public function __construct()
    {
        parent::__construct("Twitch", ['Accept', "application/vnd.twitchtv.v3+json"], StreamingSiteTypeEnum::TWITCH());
        $this->clientID = getenv("CLIENT_ID");
        $this->clientSecret = getenv("CLIENT_SECRET");
        $this->setupLinks();
    }

    public function getTopGames()
    {
        $vars = json_decode($this->get("https://api.twitch.tv/kraken/games/top?limit=50", 300));
        $games = [];
        foreach ($vars->top as $game) {
            $games[] = new Game($game->viewers, $game->channels, $game->game->name, $game->game->box->medium);
        }
        return $games;
    }

    /**
     * @param array $criterias
     * @return array
     */
    public function getByCriteria(array $criterias, $flush)
    {
        if ($this->activeSearch === null) {
            $link = null;;
            if (array_key_exists("game", $criterias)) {
                $this->activeSearch = new ServiceSearchResults(
                    $this,
                    $criterias,
                    $this->links->getGameLink($criterias['game']['value'])
                );
            } else {
                $this->activeSearch = new ServiceSearchResults(
                    $this,
                    $criterias,
                    $this->getLinks()->getPageLink()
                );
            }
        }
        return $this->activeSearch;
    }


    private
    function rawGetLiveGame($gameName, $offset = 0, $live = false)
    {
        $streams = [];
        $offset = $offset * 100;
        if ($live === 1) $live = "true";
        $json = json_decode($this->get("https://api.twitch.tv/kraken/search/streams?q=\"" . urlencode($gameName) . "\"&limit=100&offset=$offset&type=suggest&live=$live", 60));
        foreach ($json->streams as $stream) {
            $stream = new TwitchStream($this->getType(), $stream);
            Cache::put("stream_" . $stream->channel->getName(), $stream, $this->channelStoreCacheMinutes);
            if (strpos(strtolower($stream->game), strtolower($gameName)) !== false) {
                $streams[] = $stream;
            }
        }
        return $streams;
    }

    private
    function rawGrabStream($streamName)
    {
        return Cache::get($streamName, function () use ($streamName) {
            $json = json_decode($this->get("https://api.twitch.tv/kraken/streams/$streamName"));
            $channel = new TwitchStream($this->getType(), $json->stream);
            return $channel;
        });
    }

    private
    function getRawPage($page, $debug = false, $flush = false)
    {
        $offset = $page * 100;
        $json2 = json_decode($this->get("https://api.twitch.tv/kraken/streams?limit=100&offset=$offset", 10, $flush));
        if ($debug) {
            echo $page;
        }
        if (count($json2->streams) == 0) {
            throw new \Exception("No more streams to search");
        }
        return $json2->streams;
    }

    public
    function getUser($token)
    {
        $url = "https://api.twitch.tv/kraken?oauth_token=$token";
        $result = json_decode($this->get($url));
        return $result;
    }

    public function getStreams($results)
    {
        $return = [];
        foreach ($results->streams as $stream) {
            $return[] = new TwitchStream(StreamingSiteTypeEnum::TWITCH(), $stream);
        }
        return $return;
    }


    private function setupLinks()
    {
        $this->links = new StreamingLinks();
        $this->links->setSummaryLink(new StreamingLink(5, null, "https://api.twitch.tv/kraken/streams/summary"));
        $this->links->setChannelLink(new StreamingLink(1, null, "https://api.twitch.tv/kraken/channels/@channel_name"));
        $this->links->setGetChattersInStreamLink(new StreamingLink(2, null, "https://tmi.twitch.tv/group/user/@channel_name/chatters"));
        $this->links->setGameLink(new StreamingLink(2, new PageLink(), "https://api.twitch.tv/kraken/streams/?game=@game_name&limit=100&type=suggest&live=true"));
        $this->links->setStreamLink(new StreamingLink(2, null, "https://api.twitch.tv/kraken/streams/@channel_name"));
        $this->links->setTopGamesLink(new StreamingLink(5, new PageLink(), "https://api.twitch.tv/kraken/streams?limit=100"));
        $this->links->setUserLink(new StreamingLink(2, null, "https://api.twitch.tv/kraken/users/@username"));
        $this->links->setUsersFollows(new StreamingLink(2, null, "https://api.twitch.tv/kraken/users/@username/follows/channels?limit=250"));
        $this->links->setPageLink(new StreamingLink(2, new PageLink(), "https://api.twitch.tv/kraken/streams?limit=100"));
    }

    public function getUserFollows($user, $token = null)
    {
        $savedStreams = [];
        $offset = 0;
        $streams = json_decode($this->get('https://api.twitch.tv/kraken/streams/followed?limit=100', 1, false, ['Authorization', 'OAuth ' . $token]));
        $savedStreams = array_merge($savedStreams, $streams->streams);
        while (sizeof($streams->streams) != 0) {
            $offset++;
            $link = 'https://api.twitch.tv/kraken/streams/followed?limit=100&offset=' . $offset;
            $streams = json_decode($this->get('https://api.twitch.tv/kraken/streams/followed?limit=100&offset=' . $offset * 100, 1, false, ['Authorization', 'OAuth ' . $token]));
            $savedStreams = array_merge($savedStreams, $streams->streams);
        }
        $streamObj = [];
        foreach ($savedStreams as $stream) {
            $streamObj[] = new TwitchStream(StreamingSiteTypeEnum::TWITCH(), $stream);
        }
        return $streamObj;
    }

    public function getRawStreams(array $names)
    {
        $link = "https://api.twitch.tv/kraken/streams?limit=100&stream_type=live&channel=";
        foreach ($names as $name) {
            $link .= $name . ",";
        }
        $link = substr($link, 0, strlen($link) - 1);
        $results = $this->get($link, 300);
        return json_decode($results);
    }

}

class PageLink implements NextInterface
{


    /**
     * @param StreamingLink $StreamingLink
     * @param null $results
     * @param array $vars
     * @return StreamingLink
     */
    public function getNextLink($StreamingLink, $results = null, $vars = null)
    {
        return new StreamingLink(2, new PageLink(), $results->_links->next);
    }
}
