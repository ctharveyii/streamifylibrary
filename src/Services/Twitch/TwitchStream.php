<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/15/15
 * Time: 12:48 AM
 */

namespace StreamifyLibrary\Services\Twitch;


use StreamifyLibrary\Services\StreamingSiteTypeEnum;
use StreamifyLibrary\StreamStructure\Stream;

class TwitchStream extends Stream
{


    /**
     * TwitchStream constructor.
     * @param StreamingSiteTypeEnum $connection
     * @param $json
     */
    public function __construct(StreamingSiteTypeEnum $connection, $json)
    {
        parent::__construct($connection, $json);
        if (!$this->online) {
            return;
        }
        $this->preview = new Preview($json->preview->small, $json->preview->medium, $json->preview->large);
        $this->channel = new TwitchChannel($this->getType(), $json->channel);
        $this->name = $json->channel->display_name;
        $this->viewers = $json->viewers;
        $this->game = $json->game;
        $this->timeStarted = $json->created_at;
        $this->videoHeight = $json->video_height;
        $this->fps = ($json->average_fps);
    }
}