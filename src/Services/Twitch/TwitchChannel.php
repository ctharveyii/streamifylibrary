<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/23/15
 * Time: 5:23 AM
 */

namespace StreamifyLibrary\Services\Twitch;


use StreamifyLibrary\StreamStructure\Channel;
use StreamifyLibrary\StreamStructure\ChannelImages;
use StreamifyLibrary\StreamStructure\ChannelInfo;

class TwitchChannel extends Channel
{

    public function __construct($type, $json)
    {
        parent::__construct($type, $json);
        try {
            $this->name = $json->name;
            $this->link = $this->getData($json, "url", "http://www.twitch.tv/$this->name");
            $this->status = $this->getData($json, "status");


            $this->createdAt = $json->created_at;
            $this->updatedAt = $json->updated_at;
            $this->views = $this->getData($json, "views", -1);
            $this->followers = $this->getData($json, "followers", -1);

//            $this->speedrunner = Twitch\Criteria\SpeedRunningCriteria::isSpeedRunnerFromChannel($this);

            $this->channelInfo = new ChannelInfo();
            $this->channelInfo->setLanguage($this->getData($json, "language", "NONE"));
            $this->channelInfo->setMature($this->getData($json, "mature", false));
            $this->channelInfo->setPartner($this->getData($json, "partner", false));


            $this->channelImages = new ChannelImages();
            $this->channelImages->setBackground($this->getData($json, "background"));
            $this->channelImages->setBanner($this->getData($json, "profile_banner", ""));
            $this->channelImages->setLogo($this->getData($json, "logo", ""));
            $this->channelImages->setProfileBannerBackgroundColor($this->getData($json, "profile_banner_background_color", ""));
            $this->channelImages->setVideoBanner($this->getData($json, "video_banner", ""));
        } catch (\ErrorException  $ex) {
            echo "<pre>" . var_dump($ex) . "</pre>";
        }

    }

//    public function getViewers()
//    {
//        $this->viewers = new Viewers($this, $this->twitch->getChatters($this->name, 1));
//        return $this->viewers;
//    }


}