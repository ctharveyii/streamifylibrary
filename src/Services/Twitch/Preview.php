<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 4/23/15
 * Time: 8:45 AM
 */

namespace StreamifyLibrary\Services\Twitch;


use Doctrine\Common\Collections\ArrayCollection;

class Preview extends ArrayCollection
{

    public $small;

    public $medium;

    public $large;

    public function __construct($small, $medium, $large)
    {
        parent::__construct((array)$this);
        $this->small = $small;
        $this->medium = $medium;
        $this->large = $large;
    }

    public function getImage($width, $height)
    {
        return "http://static-cdn.jtvnw.net/previews-ttv/live_user_suntouch-" . $width . "x" . $height . ".jpg";
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return (array)$this;
    }
}