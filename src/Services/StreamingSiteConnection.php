<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/14/15
 * Time: 9:48 PM
 */

namespace StreamifyLibrary\Services;


use Exception;
use StreamifyLibrary\Cache;
use StreamifyLibrary\LinkProvider;
use StreamifyLibrary\Stream;
use StreamifyLibrary\Services\Twitch\TwitchChannel;
use Carbon\Carbon;

abstract class StreamingSiteConnection
{
    protected $name;
    /**
     * @var array [header part1, header part2]
     */
    protected $header = array();

    /**
     * @var StreamingLinks
     */
    protected $links;

    protected $type;

    protected $activeSearch;

    public function __construct($name, $header, StreamingSiteTypeEnum $type)
    {
        $this->name = $name;
        $this->header = $header;
        $this->type = $type;
    }

    public abstract function getByCriteria(array $criterias, $flush);

//    public abstract function getOnlineStreams($count);

    public function getStream($streamName, $flush = false)
    {
        try {
            if ($flush) {
                Cache::forget($this->getCache("stream") . $streamName);
                $link = $this->links->getStreamLink($streamName);
                $stream = json_decode($this->get($link->getLink()));
                $stream = $this->type->getStream($streamName, $stream);
                Cache::put($stream, new \DateInterval($link->getCacheTime() . "s"));
                return $stream;
            }
            return Cache::get($this->getCache("stream") . $streamName, function () use ($streamName) {
                $link = $this->links->getStreamLink($streamName);
                $stream = json_decode($this->get($link->getLink()));
                $stream = $this->type->getStream($streamName, $stream);
                Cache::put($stream, new \DateInterval($link->getCacheTime() . "s"));
                return $stream;
            });
        } catch (Exception $ex) {
            var_dump($ex);
            return [];
        }
    }


    /**
     * @param $channelName
     * @param $streamingSiteType StreamingSiteTypeEnum
     * @param bool $flush
     * @return TwitchChannel
     */
    public function getChannel($channelName, $streamingSiteType, $flush = false)
    {
        if ($flush) {
            Cache::forget($this->getCache("channel") . $channelName);
        }
        return Cache::get($this->getCache("channel") . $channelName, function () use ($channelName, $streamingSiteType) {
            $link = $this->links->getChannelLink($channelName);
            if ($link == null) return null;
            $json = json_decode($this->get($link->getLink(), $link->getCacheTime()));
            $channel = $streamingSiteType->getChannel($json, $this);
            Cache::put($this->getCache("channel") . $channelName, $channel, Carbon::now()->addSeconds($link->getCacheTime()));
            return $channel;
        });
    }

//    public function getGame($gameName)
//    {
//        return Cache::get($this->getCache("game") . $gameName, function () use ($gameName) {
//            $link = $this->links->getGameLink($gameName);
//            if ($link == null) return null;
//
//            $search = [];
//            $json = json_decode($this->get($link->getLink(), $link->getCacheTime()));
//            foreach ($json->channels as $channel) {
//                $channelObj = $this->type->getChannel($channel);
//                Cache::put($this->getCache("channel") . $channelObj->getName(), $channelObj, $this->links->getChannelLink($channelObj->name)->getCacheTime());
//                $search[] = $channelObj->toArray();
//            }
//            Cache::put($this->getCache("game") . $gameName, $search, Carbon::now()->addSeconds($link->getCacheTime()));
//            return $search;
//        });
//    }

    public function getUserByName($username)
    {
        return Cache::get($this->getCache("user") . $username, function () use ($username) {
            $link = $this->links->getUserLink($username);
            if ($link == null) return null;

            $results = json_decode($this->get($link->getLink(), $link->getCacheTime()));
            Cache::put($this->getCache("user"), $results, Carbon::now()->addMinute(5));
            return $results;
        });

    }

    public
    function getUserFollows($username, $token = null)
    {
        $link = $this->links->getUsersFollows($username);
        $results = json_decode($this->get($link->getLink(), $link->getCacheTime()));
        return $results;
    }

    public function getChatters($channelName)
    {
        return Cache::get($this->name + "_chatters_" + $channelName, function () use ($channelName) {
            $link = $this->links->getGetChattersInStreamLink($channelName);
            if ($link == null) return null;

            $results = json_encode($this->get($link->getLink(), $link->getCacheTime()));
            Cache::put($this->name . "_chatters_" . $channelName, Carbon::now()->addMinutes(1));
            return $results->chatters;
        }, 60);
    }


    /**
     * This call is a laravel cached curl call that will store the results for specified time.
     *
     * @param string $string
     * @param int $time
     * @param bool $flush
     * @return \stdClass
     */
    public
    function get($string, $time = 1, $flush = false, $headers = null, $sleep = false)
    {
        return LinkProvider::getLink($string, $this->header, $flush, $time);
    }

    public function getCache($type)
    {
        return $this->name . "_" . $type . "_";
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return StreamingSiteTypeEnum
     */
    public function getType()
    {
        return $this->type;
    }


    public
    function matchesCriterias($stream, $criterias)
    {
        foreach ($criterias as $criteria) {
            $value = $criteria['value'];
            $criteria = $criteria['criteria'];
            $criteria->resetCount();
            /** @var CriteriaBase $criteria */
            if (!$criteria->preMeetsCriteria($stream, $value)) {
                /* @var $criteria MatureCriteria */
                return false;
            }
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getActiveSearch()
    {
        return $this->activeSearch;
    }

    /**
     * @return StreamingLinks
     */
    public function getLinks()
    {
        return $this->links;
    }

    public abstract function getStreams($results);

    public abstract function getRawStreams(array $names);

    public abstract function getTopGames();


}