<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/14/15
 * Time: 10:48 PM
 */

namespace StreamifyLibrary\Services;


use StreamifyLibrary\Services\HitBox\HitBoxChannel;
use StreamifyLibrary\Services\HitBox\HitBoxStream;
use StreamifyLibrary\Services\HitBox\HitboxService;
use StreamifyLibrary\Services\Twitch\TwitchService;
use StreamifyLibrary\Services\Twitch\TwitchChannel;
use StreamifyLibrary\Services\Twitch\TwitchStream;
use StreamifyLibrary\Services\Youtube\YoutubeService;
use MyCLabs\Enum\Enum;

class StreamingSiteTypeEnum extends Enum
{
    /**
     * @method static StreamingSiteTypeEnum TWITCH()
     * @method static StreamingSiteTypeEnum HITBOX()
     * @method static StreamingSiteTypeEnum YOUTUBE()
     **/

    const TWITCH = 0;
    const HITBOX = 1;
    const YOUTUBE = 2;

    public static function getStreamingSiteByValue($value)
    {
        switch ($value) {
            case 0:
                return new TwitchService();
            case 1:
                return new HitboxService();
            case 2:
                return new YoutubeService();
        }
    }


    public function getStreamingSite()
    {
        switch ($this->getValue()) {
            case 0:
                return new TwitchService();
            case 1:
                return new HitboxService();
            case 2:
                return new YoutubeService();
        }
    }


    /**
     * @param $json
     * @return Channel
     */
    public function getChannel($json)
    {
        switch ($this->getValue()) {
            case 0:
                return new TwitchChannel($this, $json);
                break;
            case 1:
                return new HitBoxChannel($this, $json->livestream[0]);
                break;
            case 2:
                return new YoutubeChannel($this, $json);
        }
    }

    /**
     * @param string $criteria
     * @return StreamingSiteTypeEnum
     */
    public static function byName($criteria)
    {
        $criteria = strtoupper($criteria);
        try {
            $criteria = StreamingSiteTypeEnum::$criteria();
        } catch (\BadMethodCallException $ex) {
            return null;
        }
        return $criteria;
    }

    public function getIcon()
    {
        switch ($this->getValue()) {
            case 0:
                return '<i class="fa fa-fw fa-twitch"></i>';
            case 1:
                return '<img src="img/hitbox-icon.png" alt="hitbox" />';
            case 2:
                return "";
        }
    }

    public function getStream($streamName, $stream)
    {
        switch ($this->getValue()) {
            case 0:
                $stream = new TwitchStream($this, $stream->stream);
                if (!$stream->online) {
                    $channel = $this->getStreamingSite()->getChannel($streamName, $this);
                    $stream->channel = $channel;
                }
                return $stream;
            case 1:
                return new HitBoxStream($this, $stream->livestream[0]);
            case 2:
                return new YoutubeStream($this, $stream);
        }
    }

    public function getEmbedVideoLink($name)
    {
        switch ($this) {
            case StreamingSiteTypeEnum::TWITCH():
                $string = "http://www.twitch.tv/$name/embed?auto_play=false&autoplay=false";
                return $string;
            case StreamingSiteTypeEnum::HITBOX():
                $string = "http://www.hitbox.tv/embed/$name";
                return $string;
            case StreamingSiteTypeEnum::YOUTUBE():
                return "";
        }
    }

    public function getEmbedChatLink($name)
    {
        switch ($this) {
            case StreamingSiteTypeEnum::TWITCH():
                $string = "http://www.twitch.tv/$name/chat";
                return $string;
            case StreamingSiteTypeEnum::HITBOX():
                $string = "http://www.hitbox.tv/embedchat/$name";
                return $string;
            case StreamingSiteTypeEnum::YOUTUBE():
                return "";
        }
    }

}