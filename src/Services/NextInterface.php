<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 10/30/15
 * Time: 11:12 PM
 */

namespace StreamifyLibrary\Services;


interface NextInterface
{
    /**
     * @param StreamingLink $StreamingLink
     * @param null $results
     * @param array $vars
     * @return StreamingLink
     */
    public function getNextLink($StreamingLink, $results = null, $vars = null);

}