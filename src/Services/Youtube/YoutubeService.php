<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 11/2/15
 * Time: 1:15 AM
 */

namespace StreamifyLibrary\Services\Youtube;


use StreamifyLibrary\Services\StreamingLinks;
use StreamifyLibrary\Services\StreamingSiteConnection;
use StreamifyLibrary\Services\StreamingSiteTypeEnum;

class YoutubeService extends StreamingSiteConnection
{

    private $apiKey;

    public function __construct()
    {
        parent::__construct("Youtube", null, StreamingSiteTypeEnum::YOUTUBE());
        $this->apiKey = getenv("YOUTUBE_API_KEY");
    }

    public function getByCriteria(array $criterias, $flush)
    {
        // TODO: Implement getByCriteria() method.
    }

    public function getStreams($results)
    {
        // TODO: Implement getStreams() method.
    }

    public function getRawStreams(array $names)
    {
        // TODO: Implement getRawStreams() method.
    }

    public function getTopGames()
    {
        // TODO: Implement getTopGames() method.
    }

    private function setUpLinks()
    {
        $this->links = new StreamingLinks();
        $this->links->setAllOnlineLink(new StreamingLink(15, null,
            "https://www.googleapis.com/youtube/v3/search?
            part=snippet&
            eventType=live&
            type=video&
            videoCategoryId=20&
            maxResults=50&
            key=$this->activeSearch"
        ));
        $this->links->setUserLink(new StreamingLink(300, null, "https://developers.google.com/apis-explorer/#p/youtube/v3/youtube.channels.list?
        part=snippet,contentDetails
        &forUsername=@channel_name"));
        $this->links->setChannelLink(new StreamingLink(300, null,
            "https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername=@channel_name&key=$this->activeSearch"));
        $this->links->setStreamLink(new StreamingLink(300, null,
            "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=@video_id&key=$this->activeSearch"));
    }
}