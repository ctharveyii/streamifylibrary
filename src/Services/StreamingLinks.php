<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/14/15
 * Time: 10:01 PM
 */

namespace StreamifyLibrary\Services;


class StreamingLinks
{

    /**
     * @var StreamingLink
     */
    private $GameLink;
    /**
     * @var StreamingLink
     */
    private $UserLink;
    /**
     * @var StreamingLink
     */
    private $StreamLink;
    /**
     * @var StreamingLink
     */
    private $ChannelLink;
    /**
     * @var StreamingLink
     */
    private $TopGamesLink;
    /**
     * @var StreamingLink
     */
    private $UsersFollows;
    /**
     * @var StreamingLink
     */
    private $AllOnlineLink;
    /**
     * @var StreamingLink
     */
    private $ChattersInStreamLink;
    /**
     * @var StreamingLink
     */
    private $PageLink;
    /**
     * @var StreamingLink
     */

    private $summaryLink;

    /**
     * @param $gameName
     * @return StreamingLink
     */
    public function getGameLink($gameName)
    {
        if ($this->GameLink == null) return null;
        return $this->GameLink->setVar("@game_name", urlencode($gameName));
    }

    /**
     * @param mixed $getGameLink
     */
    public function setGameLink($getGameLink)
    {
        $this->GameLink = $getGameLink;
    }

    /**
     * @param $userName
     * @return StreamingLink
     */
    public function getUserLink($userName)
    {
        if ($this->UserLink == null) return null;
        return $this->UserLink->setVar("@username", $userName);
    }

    /**
     * @param StreamingLink $getUserLink
     */
    public function setUserLink($getUserLink)
    {
        $this->UserLink = $getUserLink;
    }

    /**
     * @param $streamName
     * @return StreamingLink
     */
    public function getStreamLink($streamName)
    {
        if ($this->StreamLink == null) return null;
        return $this->StreamLink->setVar("@channel_name", $streamName);
    }

    /**
     * @param StreamingLink $getStreamLink
     */
    public function setStreamLink($getStreamLink)
    {
        $this->StreamLink = $getStreamLink;
    }

    /**
     * @param $channelName
     * @return StreamingLink
     */
    public function getChannelLink($channelName)
    {
        if ($this->ChannelLink == null) return null;
        return $this->ChannelLink->setVar("@channel_name", $channelName);
    }

    /**
     * @param StreamingLink $getChannelLink
     */
    public function setChannelLink($getChannelLink)
    {
        $this->ChannelLink = $getChannelLink;
    }

    /**
     * @return StreamingLink
     */
    public function getTopGamesLink()
    {
        return $this->TopGamesLink;
    }

    /**
     * @param StreamingLink $getTopGamesLink
     */
    public function setTopGamesLink($getTopGamesLink)
    {
        $this->TopGamesLink = $getTopGamesLink;
    }

    /**
     * @return StreamingLink
     */
    public function getUsersFollows($username)
    {
        if ($this->UsersFollows == null) return null;
        return $this->UsersFollows->setVar("@username", $username);
    }

    /**
     * @param StreamingLink $getUsersFollows
     */
    public function setUsersFollows($getUsersFollows)
    {
        $this->UsersFollows = $getUsersFollows;
    }

    /**
     * @return StreamingLink
     */
    public function getAllOnlineLink()
    {
        return $this->AllOnlineLink;
    }

    /**
     * @param StreamingLink $allOnlineLink
     */
    public function setAllOnlineLink($allOnlineLink)
    {
        $this->AllOnlineLink = $allOnlineLink;
    }

    /**
     * @return StreamingLink
     */
    public function getSummaryLink()
    {
        return $this->summaryLink;
    }

    /**
     * @param StreamingLink $getAllOnlineLink
     */
    public function setSummaryLink($getAllOnlineLink)
    {
        $this->AllOnlineLink = $getAllOnlineLink;
    }

    /**
     * @param $streamName
     * @return StreamingLink
     */
    public function getGetChattersInStreamLink($streamName)
    {
        if ($this->ChattersInStreamLink == null) return null;
        return $this->ChattersInStreamLink->setVar("@channel_name", $streamName);
    }

    /**
     * @param StreamingLink $getChattersInStreamLink
     */
    public function setGetChattersInStreamLink($getChattersInStreamLink)
    {
        $this->ChattersInStreamLink = $getChattersInStreamLink;
    }

    public function setPageLink($callback)
    {
        $this->PageLink = $callback;
    }

    public function getPageLink()
    {
        if ($this->PageLink == null) return null;
        return $this->PageLink;
    }


}