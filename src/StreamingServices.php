<?php
/**
 * Created by PhpStorm.
 * User: ctharvey
 * Date: 7/14/15
 * Time: 11:11 PM
 */

namespace StreamifyLibrary;


use StreamifyLibrary\Game;
use StreamifyLibrary\SearchResults;
use StreamifyLibrary\Services\HitBox;
use StreamifyLibrary\Services\StreamingSiteTypeEnum;
use StreamifyLibrary\Services\Twitch\TwitchService;
use StreamifyLibrary\Stream;
use App\Models\PromotedStream;
use Cache\Adapter\Common\AbstractCachePool;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class StreamingServices
{

    public $cache;
    public $services;

    /**
     * StreamingServices constructor.
     * @param array $services
     * @param AbstractCachePool $cache
     */
    public function __construct(array $services, $cache = null)
    {
        $this->cache = $cache;
        foreach ($services as $service) {
            if (StreamingSiteTypeEnum::getStreamingSiteByValue($service) != null) {
                $this->services[$service] = StreamingSiteTypeEnum::getStreamingSiteByValue($service);
            }
        }
        if ($services == null || empty($services)) {
            foreach (StreamingSiteTypeEnum::values() as $name=>$service) {
                $this->services[$name] = StreamingSiteTypeEnum::getStreamingSiteByValue($service->getValue());
            }
        }
    }

    public function searchAll(){
        return new Search\Search($this->services, $this->cache);
    }



}